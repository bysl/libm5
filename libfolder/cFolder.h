#pragma once
#include <string>
#include <vector>
#include <list>
#include <map>

enum class eIterType { wm, jd, iter };
enum class eMapType { wm, jd, iter };
enum class euiType { jd, gy, sj, yz };
class cFolder
{
public:
	cFolder();
	static cFolder* getInstance();
	void setType(eIterType t);
	void setType(eMapType t);
	void setType(euiType t);

	bool apply(bool local);

	typedef std::list<std::string> sStrs;
	sStrs& getWdfs() { return _defaults; }
	const sStrs& getWdfs() const { return _defaults; }

	sStrs& getSmaps() { return _smaps; }
	const sStrs& getSmaps() const { return _smaps; }

	sStrs& getMaps() { return _maps; }
	const sStrs& getMaps() const { return _maps; }

	const std::string& getMusic() const { return _music; }
	const std::string& getMusic169() const { return _music169; }
	const std::string& getSound() const { return _sound; }
	const std::string& getSound169() const { return _sound169; }
	const std::string& getColor() const { return _color; }
	const std::string& getColor5() const { return _color5; }

	typedef std::map<unsigned long, std::pair<std::string, unsigned long>> sTables;
	const sTables& getTables() const { return _tables; }

private:
	eIterType _eiter = eIterType::jd;
	eMapType _emap = eMapType::jd;
	euiType _eui = euiType::jd;
	std::string _root, _root169, _rootWm, _music, _sound, _sound169, _music169, _color, _color5;
	sStrs _defaults, _smaps, _maps;
	sTables _tables;
};