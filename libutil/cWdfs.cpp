#include "cWdfs.h"

cWdfs::cWdfs()
{
	_currSmaps = new cReader;
	_currReader = new cReader;
	const auto& fol = cFolder::getInstance();
	const auto& smaps = fol->getSmaps();
	for (const auto& smap : smaps){
		_currSmaps->loadRoot(smap);
	}

	const auto& iters = fol->getWdfs();
	for (const auto& it2 : iters){
		_currReader->loadRoot(it2);
	}
}


cWdfs* cWdfs::getInstance()
{
	static cWdfs* s_group = new cWdfs();
	return s_group;
}